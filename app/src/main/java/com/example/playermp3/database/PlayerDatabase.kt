package com.example.playermp3.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.playermp3.database.daos.PlaylistsDao
import com.example.playermp3.database.daos.TracksDao
import com.example.playermp3.database.entitis.Playlist
import com.example.playermp3.database.entitis.Track

@Database(entities = [Track::class, Playlist::class], version = 1)
abstract class PlayerDatabase: RoomDatabase() {
    abstract fun playlistsDao(): PlaylistsDao
    abstract fun TracksDao(): TracksDao
    companion object {
        @Volatile
        private var INSTANCE: PlayerDatabase ?= null
        fun getDatabase(context: Context): PlayerDatabase {
            if (INSTANCE != null) {
                return INSTANCE!!
            }
            synchronized(this) {
                INSTANCE = Room.databaseBuilder(context, PlayerDatabase::class.java, "PlayerDB")
                    .fallbackToDestructiveMigration().build()
                return INSTANCE!!
            }
        }
    }
}