package com.example.playermp3.database.daos

import android.arch.persistence.room.*

@Dao
interface BaseDao<in T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg type: T): List<Long>
    @Update
    fun update(vararg type: T)
    @Delete
    fun delete(vararg type: T)
}