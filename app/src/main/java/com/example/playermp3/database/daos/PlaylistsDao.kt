package com.example.playermp3.database.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.example.playermp3.database.entitis.FullPlaylist
import com.example.playermp3.database.entitis.Playlist

@Dao
interface PlaylistsDao: BaseDao<Playlist> {
    @Query("SELECT * FROM playlist")
    fun getAllPlaylists(): LiveData<List<FullPlaylist>>
    @Transaction
    @Query("SELECT * FROM playlist WHERE id=:id")
    fun getPlaylistById(id: Long): LiveData<FullPlaylist>
}