package com.example.playermp3.database.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.example.playermp3.database.entitis.Track

@Dao
interface TracksDao: BaseDao<Track> {
    @Query("DELETE FROM track WHERE playlistId=:id")
    fun deleteTracksFromPlaylist(id: Long)
}