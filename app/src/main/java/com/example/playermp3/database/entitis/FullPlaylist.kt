package com.example.playermp3.database.entitis

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

class FullPlaylist {
    @Embedded
    var playlist: Playlist ?= null
    @Relation(parentColumn = "id", entityColumn = "playlistId")
    var tracks: List<Track> = ArrayList()
}