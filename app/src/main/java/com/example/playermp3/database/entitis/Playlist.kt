package com.example.playermp3.database.entitis

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "playlist")
data class Playlist(
    @PrimaryKey(autoGenerate = true)
    var id:Long = 0,
    var name:String = ""
)