package com.example.playermp3.database.entitis

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "track",
    foreignKeys = [ForeignKey(entity = Playlist::class, parentColumns = ["id"], childColumns = ["playlistId"], onDelete = ForeignKey.CASCADE)])
data class Track(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var playlistId: Long = 0,
    var data: String = "",
    var title: String = "",
    var album: String = "",
    var artist: String = ""
)