package com.example.playermp3.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.AudioFocusRequest
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaSessionCompat
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.util.Log
import com.example.playermp3.R
import com.example.playermp3.database.entitis.FullPlaylist
import com.example.playermp3.utils.PlayerStatus
import com.example.playermp3.utils.PlayerViewModel
import com.example.playermp3.view.activities.*
import java.io.IOException
import kotlin.random.Random

class PlayerService: Service(), MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener,
    AudioManager.OnAudioFocusChangeListener {

    private val LOG = "Media Player Error"
    private val iBinder: IBinder = LocalBinder()
    private var mediaPlayer: MediaPlayer ?= null
    private lateinit var focusRequest: AudioFocusRequest
    private lateinit var audioManager: AudioManager
    private var resumePosition = 0
    private lateinit var  viewModel: PlayerViewModel
    private lateinit var playlists: List<FullPlaylist>
    private var currentTrackIndex = -1
    private var currentPlaylistIndex = -1
    private var playOrResume = false
    private var status = PlayerStatus.PAUSED
    private var isLooped = false
    private var isShuffled = false
    private lateinit var mediaSession: MediaSessionCompat

    private var ongoingCall = false
    private var telephonyManager: TelephonyManager ?= null
    private val phoneStateListener = object: PhoneStateListener() {
        override fun onCallStateChanged(state: Int, phoneNumber: String?) {
            when(state) {
                TelephonyManager.CALL_STATE_OFFHOOK -> {
                    pauseMedia()
                    ongoingCall = true
                }
                TelephonyManager.CALL_STATE_RINGING -> {
                    pauseMedia()
                    ongoingCall = true
                }
                TelephonyManager.CALL_STATE_IDLE -> {
                    if (mediaPlayer != null) {
                        if (ongoingCall) {
                            ongoingCall = false
                            resumeMedia()
                        }
                    }
                }
            }
        }
    }


    private val becomingNoisyReciver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            pauseMedia()
            /////////////////////////////////////////////////////////
            //buildNotification()
            /////////////////////////////////////////////////////////
        }
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private fun callStateListener() {
        telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        telephonyManager!!.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE)
    }

    override fun onCreate() {
        super.onCreate()
        callStateListener()
        registerBecomingNoisyReciver()
        viewModel = PlayerViewModel(application)
        viewModel.allPlaylists.observeForever { fullPlaylists ->
            fullPlaylists?.let {
                playlists = it
            }
        }
        PlayerViewModel.getCurrentPlaylistIndex().observeForever { id ->
            id?.let {
                currentPlaylistIndex = it
            }
        }
        PlayerViewModel.getCurrentTrackIndex().observeForever { id ->
            id?.let {
                stopMedia()
                if (mediaPlayer != null) mediaPlayer!!.reset()
                currentTrackIndex = it
                initMediaPlayer()
                updateMetaData()
                buildNotification(status)
            }
        }
        PlayerViewModel.getPlayerStatus().observeForever { playerStatus ->
            playerStatus?.let {
                status = it
                if (it == PlayerStatus.PLAYING && !playOrResume) {
                    try {
                        PlayerViewModel.setTrack(0,0, playlists[0].tracks.size)
                        updateMetaData()
                        buildNotification(status)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else if (it == PlayerStatus.PLAYING && playOrResume) {
                    resumeMedia()
                    updateMetaData()
                    buildNotification(status)
                } else {
                    pauseMedia()
                    updateMetaData()
                    buildNotification(status)
                }
            }
        }
        PlayerViewModel.isLooped().observeForever { loopStatus ->
            loopStatus?.let {
                isLooped = it
            }
        }
        PlayerViewModel.isShuffled().observeForever { shuffleStatus ->
            shuffleStatus?.let {
                isShuffled = it
            }
        }
    }

    private fun updateMetaData() {
        mediaSession = MediaSessionCompat(applicationContext, "PlayerMP3")
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.image2)
        val track = playlists[currentPlaylistIndex].tracks[currentTrackIndex]
        mediaSession.setMetadata(MediaMetadataCompat.Builder()
            .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, bitmap)
            .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, track.artist)
            .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, track.album)
            .putString(MediaMetadataCompat.METADATA_KEY_TITLE, track.title)
            .build())
    }

    private fun buildNotification(playerStatus: PlayerStatus) {
        val playOrPause: Int
        val track = playlists[currentPlaylistIndex].tracks[currentTrackIndex]
        val intent = Intent(applicationContext, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(applicationContext, 1, intent, 0)
        val playPauseAction = if (playerStatus == PlayerStatus.PLAYING) {
            playOrPause = android.R.drawable.ic_media_pause
            playbackAction(PAUSE)
        } else {
            playOrPause = android.R.drawable.ic_media_play
            playbackAction(PLAY)
        }
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.image2)
        val channelID = "Player Channel"
        val notification = NotificationCompat.Builder(this, channelID)
            .setShowWhen(false)
            .setStyle(android.support.v4.media.app.NotificationCompat.MediaStyle()
                .setMediaSession(mediaSession.sessionToken)
                .setShowActionsInCompactView(0, 1, 2))
            .setLargeIcon(bitmap)
            .setSmallIcon(android.R.drawable.stat_sys_headset)
            .setContentText(track.title)
            .setContentTitle(track.artist)
            .setContentInfo(track.album)
            .setContentIntent(pendingIntent)
            .addAction(android.R.drawable.ic_media_previous, "previous", playbackAction(PREVIOUS))
            .addAction(playOrPause, "play_pause", playPauseAction)
            .addAction(android.R.drawable.ic_media_next, "next", playbackAction(NEXT))

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelID, "Player Channel", NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(NOTIFICATION_ID, notification.build())
    }

    private fun removeNotification() {
        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).cancel(NOTIFICATION_ID)
    }

    private fun handleIncomingActions(incomingAction: Intent?) {
        if (incomingAction == null || incomingAction.action == null) return
        if (incomingAction.action == PAUSE || incomingAction.action == PLAY ) {
            PlayerViewModel.changePlayerStatus()
        } else if (incomingAction.action == PREVIOUS) {
            PlayerViewModel.previousTrack()
        } else if (incomingAction.action == NEXT) {
            PlayerViewModel.nextTrack()
        }
    }

    private fun playbackAction(actionString: String): PendingIntent {
        val action = Intent(this, PlayerService::class.java)
        action.action = actionString
        return PendingIntent.getService(this, 0, action, 0)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (!requestAudioFocus()) {
            stopSelf()
        }
        handleIncomingActions(intent)
        return super.onStartCommand(intent, flags, startId)
    }

    private fun initMediaPlayer() {
        mediaPlayer = MediaPlayer()
        mediaPlayer!!.apply {
            setOnCompletionListener(this@PlayerService)
            setOnPreparedListener(this@PlayerService)
            setOnErrorListener(this@PlayerService)
            setOnInfoListener(this@PlayerService)
            reset()
            setAudioAttributes(AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).build())
        }

        try {
            mediaPlayer!!.setDataSource(playlists[currentPlaylistIndex].tracks[currentTrackIndex].data)
        } catch (e: IOException) {
            e.printStackTrace()
            stopSelf()
        } catch (e: Exception) {
            mediaPlayer!!.release()
            mediaPlayer = null
            return
        }
        playOrResume = true
        mediaPlayer!!.prepareAsync()
    }

    private fun playMedia() {
        if (!mediaPlayer!!.isPlaying) {
            mediaPlayer!!.start()
        }
    }

    private fun stopMedia() {
        if (mediaPlayer == null) {
            return
        }
        if (mediaPlayer!!.isPlaying) {
            mediaPlayer!!.stop()
        }
    }

    private fun pauseMedia() {
        if (mediaPlayer == null) {
            return
        }
        if (mediaPlayer!!.isPlaying) {
            resumePosition = mediaPlayer!!.currentPosition
            mediaPlayer!!.pause()
        }
    }

    private fun resumeMedia() {
        if (!mediaPlayer!!.isPlaying) {
            mediaPlayer!!.seekTo(resumePosition)
            mediaPlayer!!.start()
        }
    }

    private fun registerBecomingNoisyReciver() {
        val intentFilter = IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY)
        registerReceiver(becomingNoisyReciver, intentFilter)
    }

    override fun onBind(intent: Intent?) = iBinder

    override fun onCompletion(mp: MediaPlayer?) {
        if (isLooped) {
            stopMedia()
            if (mediaPlayer != null) mediaPlayer!!.reset()
            initMediaPlayer()
            return
        }
        if (isShuffled) {
            val size = playlists[currentPlaylistIndex].tracks.size
            var random = Random.nextInt(0, size - 1)
            while (true) {
                if (random != currentTrackIndex) {
                    break
                }
                random = Random.nextInt(0,size- 1)
            }
            PlayerViewModel.setTrack(currentPlaylistIndex, random, size )
            return
        }
        PlayerViewModel.nextTrack()
    }

    override fun onPrepared(mp: MediaPlayer?) {
        if (status != PlayerStatus.PAUSED) {
            playMedia()
        }
        resumePosition = 0
    }

    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        when(what) {
            MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK -> {
                Log.d(LOG, "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK $extra")
            }
            MediaPlayer.MEDIA_ERROR_SERVER_DIED -> {
                Log.d(LOG, "MEDIA ERROR SERVER DIED $extra")
            }
            MediaPlayer.MEDIA_ERROR_UNKNOWN -> {
                Log.d(LOG, "MEDIA ERROR UNKNOWN $extra")
            }
        }
        return false
    }

    override fun onInfo(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        return false
    }

    override fun onAudioFocusChange(focusChange: Int) {
        when(focusChange) {
            AudioManager.AUDIOFOCUS_GAIN -> {
                if (mediaPlayer == null) {
                    initMediaPlayer()
                }
                playMedia()
                mediaPlayer!!.setVolume(1.0f, 1.0f)
            }
            AudioManager.AUDIOFOCUS_LOSS -> {
                stopMedia()
                mediaPlayer!!.release()
                mediaPlayer = null
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                pauseMedia()
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                if (mediaPlayer!!.isPlaying) {
                    mediaPlayer!!.setVolume(0.1f, 0.1f)
                }
            }
        }
    }

    private fun requestAudioFocus(): Boolean {
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val result = if(android.os.Build.VERSION.SDK_INT >= 26) {
            val audioAttributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build()
            focusRequest = AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                .setAudioAttributes(audioAttributes)
                .setAcceptsDelayedFocusGain(true)
                .setOnAudioFocusChangeListener(this).build()
            audioManager.requestAudioFocus(focusRequest)
        } else {
            @Suppress("DEPRECATION")
            audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
        }

        return result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED
    }

    private fun removeAudioFocus(): Boolean {
        if(android.os.Build.VERSION.SDK_INT >= 26) {
            return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == audioManager.abandonAudioFocusRequest(focusRequest)
        } else {
            @Suppress("DEPRECATION")
            return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == audioManager.abandonAudioFocus(this)
        }

    }

    inner class LocalBinder: Binder() {
        fun getService() = this@PlayerService
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mediaPlayer != null) {
            stopMedia()
            mediaPlayer!!.release()
        }
        removeAudioFocus()

        telephonyManager!!.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE)
        removeNotification()
        unregisterReceiver(becomingNoisyReciver)
    }
}