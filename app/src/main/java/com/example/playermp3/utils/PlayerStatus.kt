package com.example.playermp3.utils

enum class PlayerStatus {
    PLAYING, PAUSED
}