package com.example.playermp3.utils

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.playermp3.database.PlayerDatabase
import com.example.playermp3.database.entitis.Playlist
import com.example.playermp3.database.entitis.Track
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class PlayerViewModel(application: Application): AndroidViewModel(application) {
    private val repository: Repository

    init {
        val database = PlayerDatabase.getDatabase(application)
        val playlistsDao = database.playlistsDao()
        val tracksDao = database.TracksDao()
        repository = Repository(playlistsDao, tracksDao)
    }
    private val createdPlaylistId = MutableLiveData<Long>()
    fun getCreatedPlaylistId(): LiveData<Long> = createdPlaylistId

    val allPlaylists = repository.allPlaylists
    fun getPlaylistById(id: Long) = repository.getPlaylistById(id)
    fun insertPlaylists(vararg playlist: Playlist) {
        doAsync {
            val ids = repository.insertPlaylists(*playlist)
            uiThread {
                createdPlaylistId.value = ids[0]
            }
        }
    }
    fun updatePlaylist(vararg playlist: Playlist) {
        doAsync {
            repository.updatePlaylists(*playlist)
        }
    }
    fun deletePlaylist(vararg playlist: Playlist) {
        doAsync {
            repository.deletePlaylists(*playlist)
        }
    }
    fun insertTracks(vararg track: Track) {
        doAsync {
            repository.insertTracks(*track)
        }
    }
    fun updateTracks(vararg track: Track) {
        doAsync {
            repository.updateTracks(*track)
        }
    }
    fun deleteTrack(vararg track: Track) {
        doAsync {
            repository.deleteTracks(*track)
        }
    }
    fun deleteTracksFromPlaylist(id: Long) {
        doAsync {
            repository.deleteTracksFromPlaylist(id)
        }
    }
    companion object {
        private var currentTrackIndex = MutableLiveData<Int>()
        private var currentPlaylistIndex = MutableLiveData<Int>()
        private var playerStatus = MutableLiveData<PlayerStatus>()
        private var lengthOfCurrentPlaylist = -1
        private var isLooped = MutableLiveData<Boolean>()
        private var isShuffled = MutableLiveData<Boolean>()

        fun getCurrentTrackIndex(): LiveData<Int> = currentTrackIndex
        fun getCurrentPlaylistIndex(): LiveData<Int> = currentPlaylistIndex
        fun getPlayerStatus(): LiveData<PlayerStatus> = playerStatus
        fun isLooped(): LiveData<Boolean> = isLooped
        fun isShuffled(): LiveData<Boolean> = isShuffled
        init {
            isLooped.value = false
            isShuffled.value = false
        }

        fun changeLoopStatus() {
            isLooped.value = isLooped.value!!.not()
        }

        fun changeShuffleStatus() {
            isShuffled.value = isShuffled.value!!.not()
        }

        fun setTrack(playlistId: Int, trackId: Int, lengthOfPlaylist: Int) {
            lengthOfCurrentPlaylist = lengthOfPlaylist
            if (currentPlaylistIndex.value != playlistId) {
                currentPlaylistIndex.value = playlistId
            }
            currentTrackIndex.value = trackId
            playerStatus.value = PlayerStatus.PLAYING
        }

        fun changePlayerStatus() {
            if (currentPlaylistIndex.value == null || currentTrackIndex.value == null) return
            if (playerStatus.value == PlayerStatus.PLAYING) {
                playerStatus.value = PlayerStatus.PAUSED
            } else {
                playerStatus.value = PlayerStatus.PLAYING
            }
        }

        fun nextTrack() {
            if (currentTrackIndex.value == lengthOfCurrentPlaylist - 1) {
                currentTrackIndex.value = 0
            } else {
                currentTrackIndex.value = currentTrackIndex.value!!.plus(1)
            }
        }
        fun previousTrack() {
            if (currentTrackIndex.value == 0) {
                currentTrackIndex.value = lengthOfCurrentPlaylist - 1
            } else {
                currentTrackIndex.value = currentTrackIndex.value!!.minus(1)
            }
        }
    }
}