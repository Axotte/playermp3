package com.example.playermp3.utils

import com.example.playermp3.database.daos.PlaylistsDao
import com.example.playermp3.database.daos.TracksDao
import com.example.playermp3.database.entitis.Playlist
import com.example.playermp3.database.entitis.Track

class Repository(
    private val playlistsDao:PlaylistsDao,
    private val tracksDao: TracksDao
) {
    val allPlaylists = playlistsDao.getAllPlaylists()
    fun getPlaylistById(id: Long) = playlistsDao.getPlaylistById(id)
    fun insertPlaylists(vararg playlist: Playlist) = playlistsDao.insert(*playlist)

    fun updatePlaylists(vararg playlist: Playlist) {
        playlistsDao.update(*playlist)
    }
    fun deletePlaylists(vararg playlist: Playlist) {
        playlistsDao.delete(*playlist)
    }
    fun insertTracks(vararg track: Track) {
        tracksDao.insert(*track)
    }
    fun updateTracks(vararg track: Track) {
        tracksDao.update(*track)
    }
    fun deleteTracks(vararg track: Track) {
        tracksDao.delete(*track)
    }
    fun deleteTracksFromPlaylist(id: Long) {
        tracksDao.deleteTracksFromPlaylist(id)
    }
}