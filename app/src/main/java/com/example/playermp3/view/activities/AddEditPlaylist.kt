package com.example.playermp3.view.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.EditText
import android.widget.ImageButton
import com.example.playermp3.R
import com.example.playermp3.database.entitis.Playlist
import com.example.playermp3.database.entitis.Track
import com.example.playermp3.utils.PlayerViewModel
import com.example.playermp3.view.adapters.SelectTracksRvAdapter
import org.jetbrains.anko.toast

class AddEditPlaylist : AppCompatActivity() {

    private val allTracks = ArrayList<Track>()
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: SelectTracksRvAdapter
    private lateinit var viewModel: PlayerViewModel
    private lateinit var saveButton: ImageButton
    private lateinit var playlistName: EditText
    private var currentPlaylistId: Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit_playlist)
        loadAudio()
        initLayout()
        viewModel = ViewModelProviders.of(this).get(PlayerViewModel::class.java)

        currentPlaylistId = intent.getLongExtra(ID, -1)
        if (currentPlaylistId != (-1).toLong()) {
            playlistName.isEnabled = false
            viewModel.getPlaylistById(currentPlaylistId).observe(this, Observer {fullPlaylist ->
                fullPlaylist?.let {
                    playlistName.setText(it.playlist!!.name)
                    selectExisting(it.tracks)
                }
            })
        }
    }

    private fun selectExisting(onPlaylist: List<Track>) {
        for (i in onPlaylist) {
            for (j in 0..(allTracks.size-1)) {
                if (i.data == allTracks[j].data) {
                    adapter.select(j)
                    break
                }
            }
        }
    }

    private fun loadAudio() {
        val uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0"
        val sortOrder = MediaStore.Audio.Media.TITLE + " ASC"
        val cursor = contentResolver.query(uri, null, selection, null, sortOrder)
        if(cursor != null && cursor.count > 0) {
            while (cursor.moveToNext()) {
                val data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))
                val title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
                val album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM))
                val artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                allTracks.add(Track(data = data, title = title, album = album, artist = artist))
            }
        }
        cursor?.close()
    }

    private fun initLayout() {
        adapter = SelectTracksRvAdapter(this)
        recyclerView = findViewById(R.id.rv_select_tracks)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter.setTracks(allTracks)
        saveButton = findViewById(R.id.but_save)
        playlistName = findViewById(R.id.et_playlist_name)
        adapter.listener = object: SelectTracksRvAdapter.OnItemClickListener {
            override fun onItemClick(adapterPosition: Int) {
                adapter.select(adapterPosition)
            }

        }
        saveButton.setOnClickListener {
            if (playlistName.text.toString() == "") {
                toast("Wpisz nazwę playlisy")
            } else {
                insertDataToDB()
                finish()
            }
        }
    }

    private fun insertDataToDB() {
        val selectedTracks = adapter.getSelected().toTypedArray()
        if (currentPlaylistId != (-1).toLong()) {
            viewModel.deleteTracksFromPlaylist(currentPlaylistId)
            for (i in selectedTracks) {
                i.playlistId = currentPlaylistId
            }
            viewModel.insertTracks(*selectedTracks)
        } else {
            viewModel.insertPlaylists(Playlist(name = playlistName.text.toString()))
            viewModel.getCreatedPlaylistId().observe(this, Observer { id ->
                id?.let {
                    for (i in selectedTracks) {
                        i.playlistId = it
                    }
                    viewModel.insertTracks(*selectedTracks)
                }
            })
        }

    }
}
