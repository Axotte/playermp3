package com.example.playermp3.view.activities

import android.Manifest
import android.arch.lifecycle.Observer
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ImageButton
import com.example.playermp3.R
import com.example.playermp3.service.PlayerService
import com.example.playermp3.utils.PlayerStatus
import com.example.playermp3.utils.PlayerViewModel
import com.example.playermp3.view.fragments.PlaylistsFragment
import com.example.playermp3.view.fragments.TracksFragment

const val ID = "com.example.playermp3.ID"
const val SERVICE_STATE = "com.example.playermp3.SERVICE_STATE"
const val PLAY = "com.example.playermp3.PLAY"
const val PAUSE = "com.example.playermp3.PAUSE"
const val NEXT = "com.example.playermp3.NEXT"
const val PREVIOUS = "com.example.playermp3.PREVIOUS"
const val NOTIFICATION_ID = 2137

class MainActivity : AppCompatActivity() {

    private lateinit var fabBack: FloatingActionButton
    private lateinit var fabAddPlaylist: FloatingActionButton
    private lateinit var fabEditPlaylist: FloatingActionButton
    private lateinit var butPlayPause: ImageButton
    private lateinit var butNext: ImageButton
    private lateinit var butPrevious: ImageButton
    private lateinit var butShuffle: ImageButton
    private lateinit var butRepeat: ImageButton
    private var currentPlaylistId: Long = -1
    private var isServiceBound = false
    private lateinit var playerService: PlayerService

    private val serviceConnection = object: ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            isServiceBound = false
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as PlayerService.LocalBinder
            playerService = binder.getService()
            isServiceBound = true
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getPermission()
        initLayout()
        initService()
        setObservers()
    }

    private fun setObservers() {
        PlayerViewModel.getPlayerStatus().observe(this, Observer {playerStatus ->
            playerStatus?.let {
                if (it == PlayerStatus.PLAYING) {
                    butPlayPause.setImageResource(R.drawable.but_pause)
                } else {
                    butPlayPause.setImageResource(R.drawable.but_play)
                }
            }
        })
        PlayerViewModel.isShuffled().observe(this, Observer { shuffleStatus ->
            shuffleStatus?.let {
                if (it) {
                    butShuffle.setImageResource(R.drawable.but_shuffle_clicked)
                } else {
                    butShuffle.setImageResource(R.drawable.but_shuffle)
                }
            }
        })
        PlayerViewModel.isLooped().observe(this, Observer { loopStatus ->
            loopStatus?.let {
                if (it) {
                    butRepeat.setImageResource(R.drawable.but_repeat_clicked)
                } else {
                    butRepeat.setImageResource(R.drawable.but_repeat)
                }
            }
        })
    }

    private fun initService() {
        if (!isServiceBound) {
            val intent = Intent(this, PlayerService::class.java)
            startService(intent)
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission", "Permission is granted")
                } else {
                    finishAffinity()
                }
            }
        }
    }

    private fun getPermission(){
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.i("Permission", "Permission is granted")
            } else {
                Log.i("Permission", "Permission is revoked")
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)
            }
        } else {
            Log.i("Permission", "Permission is granted")
        }
    }


    private fun initLayout() {
        fabBack = findViewById(R.id.fab_back)
        fabAddPlaylist = findViewById(R.id.fab_add_playlist)
        fabEditPlaylist = findViewById(R.id.fab_edit_playlist)
        butPlayPause = findViewById(R.id.but_play_pause)
        butNext = findViewById(R.id.but_next)
        butPrevious = findViewById(R.id.but_previous)
        butShuffle = findViewById(R.id.but_shuffle)
        butRepeat = findViewById(R.id.but_repeat)
        fabBack.hide()
        fabEditPlaylist.hide()
        supportFragmentManager.beginTransaction().add(R.id.container, PlaylistsFragment()).commit()
        setListeners()
    }

    private fun setListeners() {
        fabAddPlaylist.setOnClickListener {
            openActivity()
        }
        fabBack.setOnClickListener {
            swapToPlaylists()
        }
        fabEditPlaylist.setOnClickListener {
            openActivity()
        }
        butPlayPause.setOnClickListener {
            PlayerViewModel.changePlayerStatus()
        }
        butPrevious.setOnClickListener {
            PlayerViewModel.previousTrack()
        }
        butNext.setOnClickListener {
            PlayerViewModel.nextTrack()
        }
        butRepeat.setOnClickListener {
            PlayerViewModel.changeLoopStatus()
        }
        butShuffle.setOnClickListener {
            PlayerViewModel.changeShuffleStatus()
        }

    }

    private fun openActivity() {
        val intent = Intent(this, AddEditPlaylist::class.java).apply {
            putExtra(ID, currentPlaylistId)
        }
        startActivity(intent)
    }

    fun swapToTracks(id: Long, position: Int) {
        currentPlaylistId = id
        fabAddPlaylist.hide()
        fabEditPlaylist.show()
        fabBack.show()
        val fragment = TracksFragment()
        fragment.playlistPosition = position
        fragment.playlistId = id
        supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.right_to_left, R.anim.left_to_right)
            .replace(R.id.container, fragment).commit()
    }

    fun swapToPlaylists() {
        currentPlaylistId = -1
        fabBack.hide()
        fabEditPlaylist.hide()
        fabAddPlaylist.show()
        supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.back_left_to_right, R.anim.back_right_to_left)
            .replace(R.id.container, PlaylistsFragment()).commit()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState!!.putBoolean(SERVICE_STATE, isServiceBound)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        isServiceBound = savedInstanceState!!.getBoolean(SERVICE_STATE)
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isServiceBound) {
            unbindService(serviceConnection)
            playerService.stopSelf()
        }
    }
}