package com.example.playermp3.view.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.playermp3.R
import com.example.playermp3.database.entitis.FullPlaylist
import com.example.playermp3.database.entitis.Playlist

class PlaylistsRvAdapter(context: Context): RecyclerView.Adapter<PlaylistsRvAdapter.PlaylistsViewHolder>(){
    private var playlists = emptyList<FullPlaylist>()
    private val inflater = LayoutInflater.from(context)
    var listener: OnItemClickListener ?= null

    fun setAllPlaylists(playlists: List<FullPlaylist>) {
        this.playlists = playlists
        notifyDataSetChanged()
    }

    fun getPlaylistAt(position: Int) = playlists[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistsViewHolder {
        return PlaylistsViewHolder(inflater.inflate(R.layout.playlist_item, parent, false))
    }

    override fun getItemCount(): Int = playlists.size

    override fun onBindViewHolder(holder: PlaylistsViewHolder, position: Int) {
        holder.name.text = playlists[position].playlist!!.name
        holder.numberOfTracks.text = playlists[position].tracks.size.toString()
    }

    inner class PlaylistsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.tv_playlist_name)
        val numberOfTracks: TextView = itemView.findViewById(R.id.tv_number_of_tracks)

        init {
            itemView.setOnClickListener{
                if (listener != null && adapterPosition != RecyclerView.NO_POSITION) {
                    listener!!.onItemClick(playlists[adapterPosition], adapterPosition)
                }
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(playlist: FullPlaylist, position: Int)
    }
}