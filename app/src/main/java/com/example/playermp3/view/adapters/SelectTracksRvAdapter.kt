package com.example.playermp3.view.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.playermp3.R
import com.example.playermp3.database.entitis.Track

class SelectTracksRvAdapter(context: Context): RecyclerView.Adapter<SelectTracksRvAdapter.SelectTracksViewHolder>() {
    private var tracks = emptyList<Track>()
    private val inflater = LayoutInflater.from(context)
    var listener: OnItemClickListener ?= null
    private var isSelected = ArrayList<Boolean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectTracksViewHolder {
        return SelectTracksViewHolder(inflater.inflate(R.layout.select_track_item, parent, false))
    }

    fun setTracks(tracks: ArrayList<Track>) {
        this.tracks = tracks
        val tempIsSelected = ArrayList<Boolean>()
        for (i in tracks) {
            tempIsSelected.add(false)
        }
        isSelected = tempIsSelected
        notifyDataSetChanged()
    }

    fun select(position: Int) {
        isSelected[position] = !isSelected[position]
        notifyDataSetChanged()
    }

    fun getSelected(): ArrayList<Track> {
        val listSelected = ArrayList<Track>()
        for (i in 0..(isSelected.size-1)) {
            if (isSelected[i]) {
                listSelected.add(tracks[i])
            }
        }
        return listSelected
    }

    override fun getItemCount(): Int = tracks.size

    override fun onBindViewHolder(holder: SelectTracksViewHolder, position: Int) {
        holder.name.text = tracks[position].title
        if (isSelected[position]) {
            holder.isSelectedImage.setImageResource(R.drawable.ic_check_box)
        } else {
            holder.isSelectedImage.setImageResource(R.drawable.ic_check_box_outline)
        }
    }

    inner class SelectTracksViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.tv_selected_track_name)
        val isSelectedImage: ImageView = itemView.findViewById(R.id.iv_is_selected)

        init {
            itemView.setOnClickListener{
                if (listener != null && adapterPosition != RecyclerView.NO_POSITION) {
                    listener!!.onItemClick(adapterPosition)
                }
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(adapterPosition: Int)
    }
}