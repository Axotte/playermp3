package com.example.playermp3.view.adapters

import android.content.Context
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.playermp3.R
import com.example.playermp3.database.entitis.Track

class TracksRvAdapter(context: Context): RecyclerView.Adapter<TracksRvAdapter.TracksViewHolder>() {
    private var tracks = emptyList<Track>()
    private val inflater = LayoutInflater.from(context)
    var listener: OnItemClickListener ?= null
    private var currentPlaying = -1

    fun setTracks(tracks: List<Track>) {
        this.tracks = tracks
        notifyDataSetChanged()
    }

    fun changeCurrentPlaying(index: Int) {
        currentPlaying = index
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TracksViewHolder {
        return TracksViewHolder(inflater.inflate(R.layout.track_item, parent, false))
    }

    override fun getItemCount(): Int = tracks.size

    override fun onBindViewHolder(holder: TracksViewHolder, position: Int) {
        holder.name.text = tracks[position].title
        if (currentPlaying == position) {
            holder.isPlaying.visibility = View.VISIBLE
        } else {
            holder.isPlaying.visibility = View.INVISIBLE
        }
    }

    inner class TracksViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.tv_track_name)
        val isPlaying: ImageView = itemView.findViewById(R.id.iv_is_playing)

        init {
            itemView.setOnClickListener{
                if (listener != null && adapterPosition != RecyclerView.NO_POSITION) {
                    listener!!.onItemClick(adapterPosition, tracks.size)
                }
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int, size: Int)
    }
}