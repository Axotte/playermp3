package com.example.playermp3.view.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.playermp3.R
import com.example.playermp3.database.entitis.FullPlaylist
import com.example.playermp3.utils.PlayerViewModel
import com.example.playermp3.utils.SwipeToDeleteCallback
import com.example.playermp3.view.activities.MainActivity
import com.example.playermp3.view.adapters.PlaylistsRvAdapter


class PlaylistsFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: PlaylistsRvAdapter
    private lateinit var viewModel: PlayerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_playlists, container, false)
        recyclerView = view.findViewById(R.id.rv_playlists)
        adapter = PlaylistsRvAdapter(activity!!)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity!!)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PlayerViewModel::class.java)
        viewModel.allPlaylists.observe(this, Observer { fullPlaylists ->
            fullPlaylists?.let {
                adapter.setAllPlaylists(it)
            }
        })

        val swipeHandler = object: SwipeToDeleteCallback(activity!!) {
            override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {
                viewModel.deletePlaylist(adapter.getPlaylistAt(p0.adapterPosition).playlist!!)
            }
        }
        ItemTouchHelper(swipeHandler).attachToRecyclerView(recyclerView)

        adapter.listener = object: PlaylistsRvAdapter.OnItemClickListener {
            override fun onItemClick(playlist: FullPlaylist, position: Int) {
                (activity!! as MainActivity).swapToTracks(playlist.playlist!!.id, position)
            }

        }
    }
}
