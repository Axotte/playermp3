package com.example.playermp3.view.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.playermp3.R
import com.example.playermp3.utils.PlayerViewModel
import com.example.playermp3.view.activities.MainActivity
import com.example.playermp3.view.adapters.TracksRvAdapter


class TracksFragment : Fragment() {
    var playlistId: Long = -1
    var playlistPosition = -2
    private lateinit var adapter: TracksRvAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var playlistName: TextView
    private lateinit var viewModel: PlayerViewModel
    var currentPlayingPlaylist = -1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_tracks, container, false)
        if (playlistId == (-1).toLong()) {
            (activity!! as MainActivity).swapToPlaylists()
        }
        recyclerView = view.findViewById(R.id.rv_tracks)
        playlistName = view.findViewById(R.id.tv_current_playlist)
        adapter = TracksRvAdapter(activity!!)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity!!)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PlayerViewModel::class.java)
        viewModel.getPlaylistById(playlistId).observe(this, Observer {fullPlaylist ->
            fullPlaylist?.let {
                playlistName.text = it.playlist!!.name
                adapter.setTracks(it.tracks)
            }
        })
        PlayerViewModel.getCurrentPlaylistIndex().observe(this, Observer { currentPlaylistIndex ->
            currentPlaylistIndex?.let {
                currentPlayingPlaylist = it
                if (it != playlistPosition) {
                    adapter.changeCurrentPlaying(-1)
                }
            }
        })
        PlayerViewModel.getCurrentTrackIndex().observe(this, Observer { currentTrackIndex ->
            currentTrackIndex?.let {
                if (currentPlayingPlaylist == playlistPosition) {
                    adapter.changeCurrentPlaying(it)
                }
            }
        })

        adapter.listener = object: TracksRvAdapter.OnItemClickListener {
            override fun onItemClick(position: Int, size: Int) {
                PlayerViewModel.setTrack(playlistPosition, position, size)
            }
        }
    }
}
